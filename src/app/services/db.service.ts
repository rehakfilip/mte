import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { Settings } from '../Models/UserSettings';
import { stringify } from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  constructor(private storage: Storage) { }

  settings = new BehaviorSubject(new Settings());
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  getSettings(): Observable<Settings> {
    return this.settings.asObservable();
  }

  setSettings(name: string, region: string, realm: string){
    let settings = new Settings()
    settings.UserCharacterName = name;
    settings.UserCharacterRealm = realm;
    settings.UserCharacterRegion = region;   
    this.storage.set("UserSettings",  JSON.stringify(settings))
    this.settings.next(settings);
  }
}
