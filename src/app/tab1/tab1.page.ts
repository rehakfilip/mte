import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NameValuePair } from '../Models/NameValuePair';
import { DbService } from '../services/db.service';
import { Settings } from '../Models/UserSettings';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  ngOnInit(): void {
    this.dbService.getSettings().subscribe(set => {
      this.Settings = set;
      this.InputName = set.UserCharacterName;
      this.InputRegion = set.UserCharacterRegion;
      this.InputRealm = set.UserCharacterRealm
      if (this.InputName && this.InputRealm && this.InputRegion){
        this.getCharacterInfo();
      }
    })
  }
  Settings: Settings = new Settings();

  constructor(private http: HttpClient, private dbService: DbService) {}

  Player: RaiderIOModels.Player;
  InputRegion: string;
  InputRealm: string;
  InputName: string;
  NotFound: boolean;
  Regions: NameValuePair[] = [
    {name: "EU", value: "eu"},
    {name: "US", value: "us"}
  ]

  RealmsUS: NameValuePair[]=
  [
    { name : `illidan`, value :  `illidan`},
    { name : `frostmourne`, value :  `frostmourne`},
    { name : `mal'ganis`, value :  `mal'ganis`},
    { name : `area-52`, value :  `area 52`},
    { name : `stormrage`, value :  `stormrage`},
    { name : `tichondrius`, value :  `tichondrius`},
    { name : `sargeras`, value :  `sargeras`},
    { name : `zul'jin`, value :  `zul'jin`},
    { name : `thrall`, value :  `thrall`},
    { name : `bleeding hollow`, value :  `bleeding hollow`},
    { name : `barthilas`, value :  `barthilas`},
    { name : `ragnaros`, value :  `ragnaros`},
    { name : `azralon`, value :  `azralon`},
    { name : `kil'jaeden`, value :  `kil'jaeden`},
    { name : `proudmoore`, value :  `proudmoore`},
    { name : `turalyon`, value :  `turalyon`},
    { name : `dalaran`, value :  `dalaran`},
    { name :`burning blade`, value : `burning blade`},
    { name : `emerald dream`, value :  `emerald dream`},
    { name : `kel'thuzad`, value :  `kel'thuzad`},
    { name : `arthas`, value :  `arthas`},
    { name : `stormreaver`, value :  `stormreaver`},
    { name : `hyjal`, value :  `hyjal`},
    { name :`magtheridon`, value : `magtheridon`},
    { name : `lightbringer`, value :  `lightbringer`},
    { name :`whisperwind`, value : `whisperwind`},
    { name :`mug'thol`, value : `mug'thol`},
    { name :`spirestone`, value : `spirestone`},
    { name : `aerie peak`, value :  `aerie peak`},
    { name :`crushridge`, value : `crushridge`},
    { name :`cho'gall`, value : `cho'gall`},
    { name :`blackhand`, value : `blackhand`},
    { name :`doomhammer`, value : `doomhammer`},
    { name :`shattered hand`, value : `shattered hand`},
    { name :`khaz modan`, value : `khaz modan`},
    { name : `quel'thalas`, value :  `quel'thalas`},
    { name :`thaurissan`, value : `thaurissan`},
    { name :`icecrown`, value : `icecrown`},
    { name :`lothar`, value : `lothar`},
    { name :`azuremyst`, value : `azuremyst`},
    { name :`khaz'goroth`, value : `khaz'goroth`},
    { name :`nagrand`, value : `nagrand`},
    { name : `moon guard`, value :  `moon guard`},
    { name : `wyrmrest accord`, value :  `wyrmrest accord`},
    { name :`burning legion`, value : `burning legion`},
    { name : `nemesis`, value :  `nemesis`},
    { name :`bonechewer`, value : `bonechewer`},
    { name :`trollbane`, value : `trollbane`},
    { name :`boulderfist`, value : `boulderfist`},
    { name :`kilrogg`, value : `kilrogg`},
    { name :`ner'zhul`, value : `ner'zhul`},
    { name : `korgath`, value :  `korgath`},
    { name : `aman'thul`, value :  `aman'thul`},
    { name : `saurfang`, value :  `saurfang`},
    { name :`eredar`, value : `eredar`},
    { name :`silvermoon`, value : `silvermoon`},
    { name :`hellscream`, value : `hellscream`},
    { name :`dragonblight`, value : `dragonblight`},
    { name :`alleria`, value : `alleria`},
    { name :`aggramar`, value : `aggramar`},
    { name : `cenarius`, value :  `cenarius`},
    { name :`shadowmoon`, value : `shadowmoon`},
    { name :`llane`, value : `llane`},
    { name :`elune`, value : `elune`},
    { name :`suramar`, value : `suramar`},
    { name :`thunderlord`, value : `thunderlord`},
    { name :`vek'nilash`, value : `vek'nilash`},
    { name :`sen'jin`, value : `sen'jin`},
    { name :`shandris`, value : `shandris`},
    { name :`bladefist`, value : `bladefist`},
    { name :`uther`, value : `uther`},
    { name : `goldrinn`, value :  `goldrinn`},
    { name :`skullcrusher`, value : `skullcrusher`},
    { name :`windrunner`, value : `windrunner`},
    { name :`ysera`, value : `ysera`},
    { name :`jubei'thos`, value : `jubei'thos`},
    { name :`skywall`, value : `skywall`},
    { name :`alexstrasza`, value : `alexstrasza`},
    { name : `gallywix`, value :  `gallywix`},
    { name :`zuluhed`, value : `zuluhed`},
    { name :`alterac-mountains`, value : `alterac mountains`},
    { name :`korialstrasz`, value : `korialstrasz`},
    { name :`deathwing`, value : `deathwing`},
    { name :`argent-dawn`, value : `argent dawn`},
    { name :`maelstrom`, value : `maelstrom`},
    { name :`madoran`, value : `madoran`},
    { name :`kirin tor`, value : `kirin tor`},
    { name :`uldum`, value : `uldum`},
    { name :`bloodhoof`, value : `bloodhoof`},
    { name :`velen`, value : `velen`},
    { name :`nordrassil`, value : `nordrassil`},
    { name :`silver hand`, value : `silver hand`},
    { name :`ghostlands`, value : `ghostlands`},
    { name :`anvilmar`, value : `anvilmar`},
    { name : `drakkari`, value :  `drakkari`},
    { name :`shu'halo`, value : `shu'halo`},
    { name :`shadowsong`, value : `shadowsong`},
    { name : `blackrock`, value :  `blackrock`},
    { name :`draenor`, value : `draenor`},
    { name : `darkspear`, value :  `darkspear`},
    { name :`frostwolf`, value : `frostwolf`},
    { name :`thunderhorn`, value : `thunderhorn`},
    { name :`kargath`, value : `kargath`},
    { name : `garona`, value :  `garona`},
    { name :`feathermoon`, value : `feathermoon`},
    { name :`terenas`, value : `terenas`},
    { name : `earthen-ring`, value :  `earthen ring`},
    { name :`drenden`, value : `drenden`},
    { name :`rexxar`, value : `rexxar`},
    { name :`shadow council`, value : `shadow council`},
    { name :`mannoroth`, value : `mannoroth`},
    { name : `garrosh`, value :  `garrosh`},
    { name : `tol-barad`, value :  `tol barad`},
    { name :`perenolde`, value : `perenolde`},
    { name :`cenarion-circle`, value : `cenarion circle`},
    { name :`ravenholdt`, value : `ravenholdt`},
    { name :`exodar`, value : `exodar`},
    { name :`ravencrest`, value : `ravencrest`},
    { name :`moonrunner`, value : `moonrunner`},
    { name :`greymane`, value : `greymane`}

  ]

  RealmsEU: NameValuePair[] = [
    { name : 'aerie-peak', value :  'aerie-peak' },
    { name : 'aggramar', value :  'aggramar' },
    { name : `al'akir`, value :  'alakir' },
    { name : 'alonsus', value :  'alonsus' },
    { name : 'arathor', value :  'arathor' },
    { name : 'argent-dawn ', value :  'argent-dawn ' },
    { name : 'auchindoun', value :  'auchindoun' },
    { name : 'azjol-nerub', value :  'azjol-nerub' },
    { name : 'bloodhoof', value :  'bloodhoof' },
    { name : 'burning-legion ', value :  'burning-legion ' },
    { name : 'chamber-of-aspects ', value :  'chamber-of-aspects ' },
    { name : 'darksorrow', value :  'darksorrow' },
    { name : 'darkspear', value :  'darkspear' },
    { name : 'defias-brotherhood', value :  'defias-brotherhood' },
    { name : 'doomhammer', value :  'doomhammer' },
    { name : 'dragonblight', value :  'dragonblight' },
    { name : `drak'thul`, value :  'drakthul' },
    { name : 'earthen-ring', value :  'earthen-ring' },
    { name : 'emerald-dream', value :  'emerald-dream' },
    { name : 'eonar', value :  'eonar' },
    { name : 'frostmane ', value :  'frostmane ' },
    { name : 'frostwhisper', value :  'frostwhisper' },
    { name : 'grim-batol', value :  'grim-batol' },
    { name : 'kazzak ', value :  'kazzak ' },
    { name : 'kilrogg', value :  'kilrogg' },
    { name : 'lightbringer', value :  'lightbringer' },
    { name : 'magtheridon ', value :  'magtheridon ' },
    { name : 'moonglade', value :  'moonglade' },
    { name : 'nordrassil', value :  'nordrassil' },
    { name : 'outland ', value :  'outland ' },
    { name : 'ragnaros ', value :  'ragnaros ' },
    { name : 'ravencrest ', value :  'ravencrest ' },
    { name : 'shadowsong', value :  'shadowsong' },
    { name : 'shattered-hand', value :  'shattered-hand' },
    { name : 'silvermoon ', value :  'silvermoon ' },
    { name : 'stormrage', value :  'stormrage' },
    { name : 'stormreaver', value :  'stormreaver' },
    { name : 'stormscale ', value :  'stormscale ' },
    { name : 'sunstrider', value :  'sunstrider' },
    { name : 'sylvanas ', value :  'sylvanas ' },
    { name : 'tarren-mill', value :  'tarren-mill' },
    { name : 'the-maelstrom', value :  'the-maelstrom' },
    { name : 'thunderhorn', value :  'thunderhorn' },
    { name : `twilight's-hammer`, value :  `twilights-hammer` },
    { name : 'twisting-nether ', value :  'twisting-nether ' }
  ]

  msToTime(duration: number) {

    seconds = Math.floor((duration / 1000) % 60),
      minutes = Math.floor((duration / (1000 * 60)) % 60),
      hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

    var hours = (hours < 10) ? "0" + hours : hours;
    var minutes = (minutes < 10) ? "0" + minutes : minutes;
    var seconds = (seconds < 10) ? "0" + seconds : seconds;
    return hours + ":" + minutes + ":" + seconds;
  }
  updateRealm(e: any){
    if(e === "us") this.InputRealm = this.RealmsUS[0].value
    if(e === 'eu') this.InputRealm = this.RealmsEU[0].value
  }
  getCharacterInfo() {
    this.http.get<RaiderIOModels.Player>(`https://raider.io/api/v1/characters/profile?region=${this.InputRegion}&realm=${this.InputRealm}&name=${this.InputName}&fields=${encodeURIComponent("guild,raid_progression,mythic_plus_scores,mythic_plus_ranks,mythic_plus_recent_runs,mythic_plus_best_runs,mythic_plus_highest_level_runs,mythic_plus_weekly_highest_level_runs,mythic_plus_previous_weekly_highest_level_runs,previous_mythic_plus_ranks,raid_achievement_curve,raid_achievement_meta")}`)
      .toPromise()
      .then(data => {
        this.Player = data;
        this.NotFound = undefined;
      })
      .catch(error => {
        this.NotFound = error.status == 400;
        this.Player = undefined;
      });
  }
}
