import { Component, OnInit } from '@angular/core';
import { DbService } from '../services/db.service';
import { Settings } from '../Models/UserSettings';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-tab4',
  templateUrl: 'tab4.page.html',
  styleUrls: ['tab4.page.scss']
})

export class Tab4Page implements OnInit{
  constructor(private dbService: DbService){
  }
  Settings: Settings;
  UserCharacterName: string;
  UserCharacterRegion: string;
  UserCharacterRealm: string;

  ngOnInit(): void {
    this.dbService.getSettings().toPromise().then((val) => {
      this.UserCharacterName = val.UserCharacterName;
      this.UserCharacterRegion = val.UserCharacterRegion;
      this.UserCharacterRealm = val.UserCharacterRealm;
    });
  }
  setUserSettings(){
    this.dbService.setSettings(this.UserCharacterName, this.UserCharacterRegion, this.UserCharacterRealm);
  }
}
