import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Affix } from 'src/app/Models/affix';

@Component({
  selector: 'app-image-checkbox',
  templateUrl: './image-checkbox.component.html',
  styleUrls: ['./image-checkbox.component.scss'],
})
export class ImageCheckboxComponent {
  @Input() imageURL: string;
  @Input() name: string;
  @Input() isChecked: boolean;
  
  @Output() valueChange = new EventEmitter<Affix>();

  emitChanges(value: boolean): void {
    this.valueChange.emit({ isChecked: value, name: this.name, url : this.imageURL })
  }
}
