import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Affix } from '../Models/affix';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  Page: MPlusRankings.Page;
  NotFound: boolean = false;

  AffixListRow1: Affix[] = 
  [
    {isChecked : true, name : 'awakened', url : 'https://wow.zamimg.com/images/wow/icons/large/trade_archaeology_nerubian_obelisk.jpg'},
    {isChecked : false, name : 'grievous', url : 'https://wow.zamimg.com/images/wow/icons/large/ability_backstab.jpg'},
    {isChecked : false, name : 'fortified', url : 'https://wow.zamimg.com/images/wow/icons/large/ability_toughness.jpg'},
    {isChecked : false, name : 'beguiling', url : 'https://wow.zamimg.com/images/wow/icons/large/spell_shadow_mindshear.jpg'}
  ];
  
  AffixListRow2: Affix[] = 
  [
    {isChecked : false, name : 'bolstering', url : 'https://wow.zamimg.com/images/wow/icons/large/ability_warrior_battleshout.jpg'},
    {isChecked : false, name : 'skittish', url : 'https://wow.zamimg.com/images/wow/icons/large/spell_magic_lesserinvisibilty.jpg'},
    {isChecked : false, name : 'necrotic', url : 'https://wow.zamimg.com/images/wow/icons/large/spell_deathknight_necroticplague.jpg'},
    {isChecked : false, name : 'bursting', url : 'https://wow.zamimg.com/images/wow/icons/large/ability_ironmaidens_whirlofblood.jpg'}
  ]

  AffixListRow3: Affix[] = 
  [
    {isChecked : false, name : 'sanguine', url : 'https://wow.zamimg.com/images/wow/icons/large/spell_shadow_bloodboil.jpg'},
    {isChecked : false, name : 'tyrannical', url : 'https://wow.zamimg.com/images/wow/icons/large/achievement_boss_archaedas.jpg'},
    {isChecked : false, name : 'volcanic', url : 'https://wow.zamimg.com/images/wow/icons/large/spell_shaman_lavasurge.jpg'},
    {isChecked : false, name : 'quaking', url : 'https://wow.zamimg.com/images/wow/icons/large/spell_nature_earthquake.jpg'}
  ]

  AffixListRow4: Affix[] = [
    {isChecked : false, name : 'explosive', url : 'https://wow.zamimg.com/images/wow/icons/large/spell_fire_felflamering_red.jpg'},
    {isChecked : false, name : 'teeming', url : 'https://wow.zamimg.com/images/wow/icons/large/spell_nature_massteleport.jpg'},
    {isChecked : false, name : 'reaping', url : 'https://wow.zamimg.com/images/wow/icons/large/ability_racial_embraceoftheloa_bwonsomdi.jpg'},
    {isChecked : false, name : 'raging', url : 'https://wow.zamimg.com/images/wow/icons/large/ability_warrior_focusedrage.jpg'}
  ]
  affixes = 'awakened';
  
  constructor(private http: HttpClient) {  }

  getRankings() {
    let affixesURLSafe = encodeURIComponent(this.affixes.split('-').length != 4 ? 'current' : this.affixes);
    this.http.get<MPlusRankings.Page>(`https://raider.io/api/v1/mythic-plus/runs?season=season-bfa-4&region=eu&dungeon=all&affixes=${affixesURLSafe}`)
      .toPromise()
      .then(data => {
        if(data.rankings.length == 0){
          this.Page = undefined;
          this.NotFound = true;
          return;
        }
        this.NotFound = undefined;
        this.Page = data;
         })
      .catch(error => {
        this.NotFound = error.status == 400
        this.Page = undefined;
      });
    }
    msToTime(duration: number) {

      seconds = Math.floor((duration / 1000) % 60),
        minutes = Math.floor((duration / (1000 * 60)) % 60),
        hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
  
      var hours = (hours < 10) ? "0" + hours : hours;
      var minutes = (minutes < 10) ? "0" + minutes : minutes;
      var seconds = (seconds < 10) ? "0" + seconds : seconds;
      return hours + ":" + minutes + ":" + seconds;
    }

    prepareAffixList(affix: Affix): void {
      if (this.affixes.split('-').length > 4) {
        return;
      }
      if(affix.name == 'awakened') return;
      if (affix.isChecked) {
        this.affixes = `${this.affixes}-${affix.name}`
      } else {
        this.affixes = this.affixes.replace(`-${affix.name}`, '');
      }
    }
}