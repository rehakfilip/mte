declare module RaiderIOModels {

    export interface Overall {
        world: number;
        region: number;
        realm: number;
    }

    export interface Class {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionOverall {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionClass {
        world: number;
        region: number;
        realm: number;
    }

    export interface Dps {
        world: number;
        region: number;
        realm: number;
    }

    export interface ClassDps {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionDps {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionClassDps {
        world: number;
        region: number;
        realm: number;
    }

    export interface Spec259 {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionSpec259 {
        world: number;
        region: number;
        realm: number;
    }

    export interface Spec260 {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionSpec260 {
        world: number;
        region: number;
        realm: number;
    }

    export interface Spec261 {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionSpec261 {
        world: number;
        region: number;
        realm: number;
    }

    export interface MythicPlusRanks {
        overall: Overall;
        class: Class;
        faction_overall: FactionOverall;
        faction_class: FactionClass;
        dps: Dps;
        class_dps: ClassDps;
        faction_dps: FactionDps;
        faction_class_dps: FactionClassDps;
        spec_259: Spec259;
        faction_spec_259: FactionSpec259;
        spec_260: Spec260;
        faction_spec_260: FactionSpec260;
        spec_261: Spec261;
        faction_spec_261: FactionSpec261;
    }

    export interface Overall2 {
        world: number;
        region: number;
        realm: number;
    }

    export interface Class2 {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionOverall2 {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionClass2 {
        world: number;
        region: number;
        realm: number;
    }

    export interface Dps2 {
        world: number;
        region: number;
        realm: number;
    }

    export interface ClassDps2 {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionDps2 {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionClassDps2 {
        world: number;
        region: number;
        realm: number;
    }

    export interface Spec2592 {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionSpec2592 {
        world: number;
        region: number;
        realm: number;
    }

    export interface Spec2602 {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionSpec2602 {
        world: number;
        region: number;
        realm: number;
    }

    export interface Spec2612 {
        world: number;
        region: number;
        realm: number;
    }

    export interface FactionSpec2612 {
        world: number;
        region: number;
        realm: number;
    }

    export interface PreviousMythicPlusRanks {
        overall: Overall2;
        class: Class2;
        faction_overall: FactionOverall2;
        faction_class: FactionClass2;
        dps: Dps2;
        class_dps: ClassDps2;
        faction_dps: FactionDps2;
        faction_class_dps: FactionClassDps2;
        spec_259: Spec2592;
        faction_spec_259: FactionSpec2592;
        spec_260: Spec2602;
        faction_spec_260: FactionSpec2602;
        spec_261: Spec2612;
        faction_spec_261: FactionSpec2612;
    }

    export interface Affix {
        id: number;
        name: string;
        description: string;
        wowhead_url: string;
    }

    export interface MythicPlusRecentRun {
        dungeon: string;
        short_name: string;
        mythic_level: number;
        completed_at: Date;
        clear_time_ms: number;
        num_keystone_upgrades: number;
        map_challenge_mode_id: number;
        score: number;
        affixes: Affix[];
        url: string;
    }

    export interface Affix2 {
        id: number;
        name: string;
        description: string;
        wowhead_url: string;
    }

    export interface MythicPlusBestRun {
        dungeon: string;
        short_name: string;
        mythic_level: number;
        completed_at: Date;
        clear_time_ms: number;
        num_keystone_upgrades: number;
        map_challenge_mode_id: number;
        score: number;
        affixes: Affix2[];
        url: string;
    }

    export interface Affix3 {
        id: number;
        name: string;
        description: string;
        wowhead_url: string;
    }

    export interface MythicPlusHighestLevelRun {
        dungeon: string;
        short_name: string;
        mythic_level: number;
        completed_at: Date;
        clear_time_ms: number;
        num_keystone_upgrades: number;
        map_challenge_mode_id: number;
        score: number;
        affixes: Affix3[];
        url: string;
    }

    export interface Affix4 {
        id: number;
        name: string;
        description: string;
        wowhead_url: string;
    }

    export interface MythicPlusWeeklyHighestLevelRun {
        dungeon: string;
        short_name: string;
        mythic_level: number;
        completed_at: Date;
        clear_time_ms: number;
        num_keystone_upgrades: number;
        map_challenge_mode_id: number;
        score: number;
        affixes: Affix4[];
        url: string;
    }

    export interface BattleOfDazaralor {
        summary: string;
        total_bosses: number;
        normal_bosses_killed: number;
        heroic_bosses_killed: number;
        mythic_bosses_killed: number;
    }

    export interface CrucibleOfStorms {
        summary: string;
        total_bosses: number;
        normal_bosses_killed: number;
        heroic_bosses_killed: number;
        mythic_bosses_killed: number;
    }

    export interface NyalothaTheWakingCity {
        summary: string;
        total_bosses: number;
        normal_bosses_killed: number;
        heroic_bosses_killed: number;
        mythic_bosses_killed: number;
    }

    export interface TheEternalPalace {
        summary: string;
        total_bosses: number;
        normal_bosses_killed: number;
        heroic_bosses_killed: number;
        mythic_bosses_killed: number;
    }

    export interface Uldir {
        summary: string;
        total_bosses: number;
        normal_bosses_killed: number;
        heroic_bosses_killed: number;
        mythic_bosses_killed: number;
    }

    export interface RaidProgression {
        "battle-of-dazaralor": BattleOfDazaralor;
        "crucible-of-storms": CrucibleOfStorms;
        "nyalotha-the-waking-city": NyalothaTheWakingCity;
        "the-eternal-palace": TheEternalPalace;
        uldir: Uldir;
    }

    export interface Guild {
        name: string;
        realm: string;
    }
    export interface MythicPlusScores{
        all: number,
        dps: number,
        healer: number,
        tank: number
    }
    export interface Player {
        name: string;
        race: string;
        class: string;
        active_spec_name: string;
        active_spec_role: string;
        gender: string;
        faction: string;
        achievement_points: number;
        honorable_kills: number;
        thumbnail_url: string;
        region: string;
        realm: string;
        profile_url: string;
        profile_banner: string;
        mythic_plus_scores: MythicPlusScores;
        mythic_plus_ranks: MythicPlusRanks;
        previous_mythic_plus_ranks: PreviousMythicPlusRanks;
        mythic_plus_recent_runs: MythicPlusRecentRun[];
        mythic_plus_best_runs: MythicPlusBestRun[];
        mythic_plus_highest_level_runs: MythicPlusHighestLevelRun[];
        mythic_plus_weekly_highest_level_runs: MythicPlusWeeklyHighestLevelRun[];
        mythic_plus_previous_weekly_highest_level_runs: any[];
        raid_progression: RaidProgression;
        raid_achievement_meta: any[];
        raid_achievement_curve: any[];
        guild: Guild;
    }
}

