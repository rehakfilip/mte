declare module MPlusRankings {

    export interface Dungeon {
        id: number;
        name: string;
        short_name: string;
        slug: string;
        expansion_id: number;
        patch: string;
        keystone_timer_ms: number;
    }

    export interface WeeklyModifier {
        id: number;
        icon: string;
        name: string;
        description: string;
    }

    export interface Class {
        id: number;
        name: string;
        slug: string;
    }

    export interface Race {
        id: number;
        name: string;
        slug: string;
        faction: string;
    }

    export interface Spec {
        id: number;
        name: string;
        slug: string;
    }

    export interface Realm {
        id: number;
        connectedRealmId: number;
        name: string;
        altName: string;
        slug: string;
        altSlug: string;
        locale: string;
        isConnected: boolean;
    }

    export interface Region {
        name: string;
        slug: string;
        short_name: string;
    }

    export interface Character {
        id: number;
        persona_id: number;
        name: string;
        class: Class;
        race: Race;
        faction: string;
        level: number;
        spec: Spec;
        path: string;
        realm: Realm;
        region: Region;
    }

    export interface Roster {
        character: Character;
        oldCharacter?: any;
        isTransfer: boolean;
        role: string;
    }

    export interface Run {
        season: string;
        dungeon: Dungeon;
        keystone_run_id: number;
        keystone_team_id: number;
        keystone_platoon_id?: any;
        mythic_level: number;
        clear_time_ms: number;
        keystone_time_ms: number;
        completed_at: Date;
        num_chests: number;
        time_remaining_ms: number;
        faction: string;
        weekly_modifiers: WeeklyModifier[];
        num_modifiers_active: number;
        roster: Roster[];
        platoon?: any;
    }

    export interface Ranking {
        rank: number;
        score: number;
        run: Run;
    }

    export interface Params {
        season: string;
        region: string;
        dungeon: string;
        affixes: string;
        page: number;
    }

    export interface Page {
        rankings: Ranking[];
        leaderboard_url: string;
        params: Params;
    }

}

