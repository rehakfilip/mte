export interface Affix {
    name: string;
    isChecked: boolean;
    url: string;
}