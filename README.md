# Ionic v4 MTE SCHOOL PROJECT

Project created for school MTE course.

## Structure
The complete source code is contained within.

## How to Run Locally
* Clone this repo.
* Open a terminal window, and navigate to this repo on the filesystem.
* Run "npm install" to install all required project dependencies. 
* Run "ionic serve" to run the app in a web browser locally.
